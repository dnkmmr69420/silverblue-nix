<!--
SPDX-FileCopyrightText: 2022 Andrew Hayzen <ahayzen@gmail.com>

SPDX-License-Identifier: CC0-1.0
-->

# Example Nix config

Enable optimising of the store globally and allow unfree for this user.

```bash
$ echo "auto-optimise-store = true" | sudo tee -a /etc/nix/nix.conf
$ mkdir -p ~/.config/nixpkgs
$ echo "{ allowUnfree = true; }" > ~/.config/nixpkgs/config.nix
```

Add Nix and home-manager stable channels.

```bash
$ nix-channel --add https://nixos.org/channels/nixos-21.11 nixpkgs
$ nix-channel --add https://github.com/nix-community/home-manager/archive/release-21.11.tar.gz home-manager
$ nix-channel --update
```

Install home-manager

```bash
$ export NIX_PATH=$HOME/.nix-defexpr/channels${NIX_PATH:+:}$NIX_PATH
$ nix-shell '<home-manager>' -A install
$ echo "if [ -e $HOME/.nix-profile/etc/profile.d/hm-session-vars.sh ]; then . $HOME/.nix-profile/etc/profile.d/hm-session-vars.sh; fi" >> ~/.profile
```

Edit your home-manager config using `$ home-manager edit`, it could look like the following

```
{ config, home, pkgs, ... }:

{
  home.homeDirectory = "/var/home/username";
  home.packages = [
    pkgs.clang-tools
  ]
  home.sessionVariables = {
     EDITOR = "nano";
  };
  home.stateVersion = "21.11";
  home.username = "username";
  news.display = "silent";
  programs.home-manager.enable = true;
  programs.git = {
    enable = true;
    lfs = {
      enable = true;
    };
    userName  = "name";
    userEmail = "name@example.com";
  };
  services.syncthing.enable = true;
  targets.genericLinux.enable = true;
}
```

Tell home-manager to switch to the new config.

```bash
$ home-manager switch
```

Note that for desktop applications to appear in the launcher after installing home-manager you need to reboot for the env vars to be updated.
